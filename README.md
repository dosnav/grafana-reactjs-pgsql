# grafana-reactjs-pgsql



## Getting started

## Запуск Terraform:

```
terraform apply -auto-approve --var-file=my-var.tfvars | grep external_ip_address_vm
```

## Запуск Ansible
Для запуска ansible необходимо добавить ip адреса в файлы:

- [ ] hosts
- [ ] roles/grafana/default/main.yaml

```
ansible-playbook monitoring.yaml
```
